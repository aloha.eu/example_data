import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
from unet import UNet
from torchvision import transforms, datasets

import torch.onnx as torch_onnx


import os




device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

model = UNet(n_classes=1, padding=True, up_mode='upsample', wf=4).to(device)

dummy_input = Variable(torch.randn(1, 1,256,256))
torch_onnx.export(model, dummy_input, "./unet.onnx", verbose=False, export_params=True)



#optim = torch.optim.Adam(model.parameters())

#dataset = datasets.CIFAR10(root="./", train=True, download=True, transform=None)

#dataloader =torch.utils.data.DataLoader(dataset, shuffle=True)



#data_transform = transforms.Compose([
#        transforms.Grayscale(num_output_channels=1),
#        transforms.RandomSizedCrop(224),
#        transforms.RandomHorizontalFlip(),
#        transforms.ToTensor(),
#        transforms.Normalize(mean=[0.485, 0.456, 0.406],
#                             std=[0.229, 0.224, 0.225])
#    ])


#hymenoptera_dataset = datasets.ImageFolder(root='/home/gderiu/hymenoptera_data',
                                    #       transform=data_transform)
#dataset_loader = torch.utils.data.DataLoader(hymenoptera_dataset,
#                                             batch_size=1, shuffle=True,
#                                             num_workers=4)

#epochs = 10


#for _ in range(epochs):
#    for batch_idx, (X, y) in enumerate(dataset_loader):
#        print (X)
#        print (y)
#        #X = X.to(device)  # [N, 1, H, W]
#        #y = y.to(device)  # [N, H, W] with class indices (0, 1)
#        prediction = model(X)  # [N, 2, H, W]
#        loss = nn.CrossEntropyLoss(prediction, y)

#        optim.zero_grad()
#        #loss.backward()
#        optim.step()
