#!/bin/bash
wget https://onnxzoo.blob.core.windows.net/models/opset_8/tiny_yolov2/tiny_yolov2.tar.gz
tar -xf tiny_yolov2.tar.gz -C ./ tiny_yolov2/Model.onnx
mkdir -p ./algorithms/yolo/
mv tiny_yolov2/model.onnx ./algorithms/yolo/tiny_yolov2.onnx
rm -fr tiny_yolov2.tar.gz tiny_yolov2
