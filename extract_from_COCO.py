from pycocotools.coco import COCO
import json
import os
import shutil
from uuid import uuid4


dataset_folder = "./COCO_extract_{}".format(str(uuid4()).split("-")[0])
src_images_folder = "/home/gderiu/COCO/val2017"
dst_images_folder = os.path.join(dataset_folder,"images")
source_path = os.path.join('/opt/www/shared_data/usecases/',dataset_folder,'images')

annFile='./annotations/instances_val2017.json'
coco=COCO(annFile)

cats = coco.loadCats(coco.getCatIds())
nms=[cat['name'] for cat in cats]
print('COCO categories: \n{}\n'.format(' '.join(nms)))

nms = set([cat['supercategory'] for cat in cats])
print('COCO supercategories: \n{}'.format(' '.join(nms)))

catNms=['person','bicycle']

# set the categories you want to query
catIds = coco.getCatIds(catNms=catNms);

imgIds = coco.getImgIds(catIds=catIds );

#print (imgIds)
frames= []
for i in imgIds:
  img = coco.loadImgs(i)[0]
  #print (img)
 # exit()
  annIds = coco.getAnnIds(imgIds=img['id'], catIds=catIds, iscrowd=None)
  anns = coco.loadAnns(annIds)
  objects = []
  for a in anns:
    o = {'object_class' : a['category_id'],
         'bb'           : a['bbox'] ,
         'polygon'      : a['segmentation']}
         
    objects.append(o)
  
  frame = {"frame"   : img['file_name'],
           "width"   : img['width'],
           "height"  : img['height'],
           "mask"    : "",
           "objects" : objects}
 
  #print (frame['frame'])
  if os.path.exists(os.path.join(src_images_folder,frame['frame'])):
    frames.append(frame)

print ("\n\nFound {} images correspondig to categories: ".format(len(frames)))
print (catNms)

if source_path=='':
  source_path = dst_images_folder

dataset={"mask_path": "", "name": "tst_dataset", "source_path": source_path, "source": "image", "width": 0, "height": 0, "meta":frames}

os.makedirs(dst_images_folder, exist_ok=True)
for i in dataset['meta']:
  shutil.copy(os.path.join(src_images_folder,i['frame']), os.path.join(dst_images_folder,i['frame']))

with open(os.path.join(dataset_folder,"annotations.json"), "w") as text_file:
    text_file.write(json.dumps(dataset))

