#/usr/bin/env bash

trap "rm -f logbook.data.? logbook?.txt pareto.data perf-cost.png" INT

# Run twice with same seed, and once with a different one.
# Remove template_map.yml because reusing an old one will change the run.
make clean && rm -f template_map.yml && RunDSE --nograph -s 1 -j4 template.yml
mv logbook.data logbook.data.1

make clean && rm -f template_map.yml && RunDSE --nograph -s 1 -j4 template.yml
mv logbook.data logbook.data.2

make clean && rm -f template_map.yml && RunDSE --nograph -s 110 -j4 template.yml
mv logbook.data logbook.data.3

# Convert logbooks to text
PrintLogBook.py -n logbook.data.1 > logbook1.txt
PrintLogBook.py -n logbook.data.2 > logbook2.txt
PrintLogBook.py -n logbook.data.3 > logbook3.txt

# Check if runs with same seed are identical
echo
echo "Comparing seed 1 and seed 1 run"
diff logbook1.txt logbook2.txt
echo

echo "Comparing seed 1 and seed 110 run"
# Check if runs with different seeds differ
diff logbook1.txt logbook3.txt

# cleanup
rm -f logbook.data.? logbook?.txt pareto.data perf-cost.png > /dev/null
make clean > /dev/null

