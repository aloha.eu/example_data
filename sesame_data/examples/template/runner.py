from pysesame import sesame
from sesamemap import appyml, archyml
from sesamemap import apparchmap
from pyyml import ymlmap
import sys

project = sesame.SesameProject(sys.argv[1])

app_yml, arch_yml, map_yml = project.parse_simulation_yml()

app = appyml.Application(app_yml)
arch = archyml.Architecture(arch_yml)
m = apparchmap.AppArchMap(app, arch)
print m
map_yml = m.export_yml()
ymlmap.store_mapping('template_map.yml', map_yml)




print "Building experiment (make all)"
project.build_all()

print "Running experiment (make run)"
cycles = project.runapparch()
print "Returned cycle count is:" 
print cycles
